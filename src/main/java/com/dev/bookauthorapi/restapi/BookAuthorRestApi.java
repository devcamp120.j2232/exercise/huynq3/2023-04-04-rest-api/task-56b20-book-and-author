package com.dev.bookauthorapi.restapi;
import java.util.ArrayList;
import java.util.List;
import com.dev.bookauthorapi.Book;
import com.dev.bookauthorapi.Author;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;



@RestController
public class BookAuthorRestApi {
    @CrossOrigin
    @GetMapping("/books")
    public List<Book> getAllBooks() {
        // initialize 3 authors and print them to console
        Author author1 = new Author("Huy 18+", "huy@depchai.com",'m');
        Author author2 = new Author("Yen Vu 20+", "yen@depgai.com", 'f');
        Author author3 = new Author("Linh Sua Chua", "emhamchoi@luoihoc.com", 'l');
        System.out.println(author1);
        System.out.println(author2);
        System.out.println(author3);

        // initialize 3 books using the authors above and print them to console
        Book book1 = new Book("Songoku", author1, 1000, 5);
        Book book2 = new Book("Elsa", author2, 1200, 6);
        Book book3 = new Book("Han Quoc", author3, 1500, 7);
        System.out.println(book1);
        System.out.println(book2);
        System.out.println(book3);

        // create a new ArrayList and add the books above to it
        List<Book> books = new ArrayList<Book>();
        books.add(book1);
        books.add(book2);
        books.add(book3);

        return books;
    }


}
